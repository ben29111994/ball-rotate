﻿Shader "Hidden/GPUInstancer/HiZOcclusionDebugger"
{
    Properties
    {
       _MainTex ("Texture", 2D) = "white" {}
    }

    CGINCLUDE
    #include "UnityCG.cginc"

	struct Input
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
    };

    struct Varyings
    {
        float4 vertex : SV_POSITION;
        float2 uv : TEXCOORD0;
    };

    Texture2D _MainTex;
    SamplerState sampler_MainTex;
	float4 _MainTex_TexelSize;
	int _HiZMipLevel;

	float isMultiPassVR;

    Varyings vertex(Input input)
    {
        Varyings output;

        output.vertex = UnityObjectToClipPos(input.vertex.xyz);
        output.uv = input.uv;

		#if UNITY_UV_STARTS_AT_TOP
			if (_MainTex_TexelSize.y < 0)
				output.uv.y = 1 - input.uv.y;
		#endif
	
        return output;
    }

    float4 fragment(in Varyings input) : SV_Target
    {
		float4 eyeColor = float4(1,1,1,1);

		#if defined(MULTIPASS_VR_ENABLED) || defined(SINGLEPASS_VR_ENABLED)

			if (unity_StereoEyeIndex == 0)
			{
				eyeColor = float4(1,0,0,1);
			}

			#ifdef HIZ_TEXTURE_FOR_BOTH_EYES
				input.uv.x *= 0.5;

				if (unity_StereoEyeIndex == 1)
				{
					eyeColor = float4(0,1,0,1);
					input.uv.x += 0.5;
				}
			#else
				if (unity_StereoEyeIndex == 1)
				{
					return float4(0,0,0,1);
				}
			#endif

		#endif

		return eyeColor * (_MainTex.SampleLevel(sampler_MainTex, input.uv, _HiZMipLevel).r) * 10;
    }
    ENDCG

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
			#pragma target 3.5
            #pragma vertex vertex
            #pragma fragment fragment
			#pragma multi_compile __ HIZ_TEXTURE_FOR_BOTH_EYES
			#pragma multi_compile __ MULTIPASS_VR_ENABLED SINGLEPASS_VR_ENABLED
            ENDCG
        }
    }
}
